import React, {FC} from 'react';
import Header from "../../components/Header/Header";
import FormRegistration from "../../components/UIComponents/Form/FormRegistration/FormRegistration";

const Register:FC = () => {
    return (
        <>
            <Header />
            <FormRegistration />
        </>
    );
};

export default Register;