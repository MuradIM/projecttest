import React, {FC} from 'react';
import Header from "../../components/Header/Header";
import FormAuth from "../../components/UIComponents/Form/FormAuth/FormAuth";

const Auth: FC = () => {

    return (
        <>
            <Header/>
            <FormAuth />
        </>
    );
};

export default Auth;