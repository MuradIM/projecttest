import React, {FC} from 'react';
import {Link} from "react-router-dom";
import {BLOG_PAGE, DASHBOARD_PAGE, MARKETPLACE_PAGE, MY_INVESTMENTS_PAGE, WALLET_PAGE} from "../../utils/consts"
import styles from "./styles.module.scss"

const Menu: FC<{ from: "header" | "footer" }> = ({from}) => {

    return (
        <div className={from === "header"
            ? styles.wrapperHeader
            : styles.wrapperFooter
        }>
            <ul className={styles.list}>
                <li className={styles.item}><Link to={MARKETPLACE_PAGE}>Marketplace</Link></li>
                <li className={styles.item}><Link to={DASHBOARD_PAGE}>Dashboard</Link></li>
                <li className={styles.item}><Link to={WALLET_PAGE}>Wallet</Link></li>
                <li className={styles.item}><Link to={MY_INVESTMENTS_PAGE}>My Investments</Link></li>
                <li className={styles.item}><Link to={BLOG_PAGE}>Blog</Link></li>
            </ul>
        </div>
    );
};

export default Menu;