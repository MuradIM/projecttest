import React, {FC} from 'react';
import {useTypedSelector} from "../../../hooks/useTypedSelector";
import ToolsForAuthUser from "./ToolsForAuthUser";
import ToolsForNonAuthUser from "./ToolsForNonAuthUser";

const Tools: FC = () => {
    const {userAuth} = useTypedSelector(state => state)
    const {isAuth} = userAuth
    return (
        <>
            {isAuth
                ? <ToolsForAuthUser/>
                : <ToolsForNonAuthUser/>
            }
        </>
    );
};

export default Tools;