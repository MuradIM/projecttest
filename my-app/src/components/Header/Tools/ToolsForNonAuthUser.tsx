import React, {FC, MouseEventHandler} from 'react';
import MainButton from "../../UIComponents/Buttons/MainButton/MainButton";
import {NavigateFunction, useNavigate} from "react-router-dom";
import {AUTH_PAGE, REGISTER_PAGE} from "../../../utils/consts";
import LanguageSwitchButton from "../../UIComponents/Buttons/LanguageSwitchButton/LanguageSwitchButton";


const ToolsForNonAuthUser: FC = () => {
    const navigate:NavigateFunction = useNavigate()

    const handleLog:MouseEventHandler = ():void => {
        navigate(`${AUTH_PAGE}`)
    }

    const handleRegister:MouseEventHandler = ():void => {
        navigate(`${REGISTER_PAGE}`)
    }

    return (
        <div>
            <LanguageSwitchButton />
            <MainButton sx={{marginRight: "32px", fontSize: "16px"}} modify="text" onClick={handleRegister}>Register</MainButton>
            <MainButton sx={{marginRight: "60px", fontSize: "16px"}} modify="contained" onClick={handleLog}>Log in</MainButton>
        </div>
    );
};

export default ToolsForNonAuthUser