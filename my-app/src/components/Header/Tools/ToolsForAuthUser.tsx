import React, {FC} from 'react';
import LanguageSwitchButton from "../../UIComponents/Buttons/LanguageSwitchButton/LanguageSwitchButton";
import NotificationButton from "../../UIComponents/Buttons/NotificationButton/NotificationButton";
import DropdownProfileMenuButton from "../../UIComponents/Buttons/DropdownProfileMenuButton/DropdownProfileMenuButton";
import UserPhotoItem from "../../UIComponents/UserPhotoItem/UserPhotoItem";

const style: any = {
    display: "flex",
    alignItems: "center",
}

const ToolsForAuthUser: FC = () => {
    return (
        <div style={style}>
            <NotificationButton/>
            <LanguageSwitchButton/>
            <UserPhotoItem/>
            <DropdownProfileMenuButton/>
        </div>
    );
};

export default ToolsForAuthUser