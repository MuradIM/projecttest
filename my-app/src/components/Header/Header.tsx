import React, {FC} from 'react';
import Tools from "./Tools/Tools";
import Menu from "../SiteMap/Menu";
import Logo from "../UIComponents/Logo/Logo";
import styles from "./styles.module.scss"

const Header: FC = () => {

    return (
        <header className={styles.header}>
            <Logo/>
            <Menu from={"header"}/>
            <Tools/>
        </header>
    );
};

export default Header;