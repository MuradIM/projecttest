import React, {FC} from 'react';
import styles from "./styles.module.scss"

const RememberMeLabel:FC = () => {
    return (
        <span className={styles.text}>
            remember me
        </span>
    );
};

export default RememberMeLabel;