import React, {FC} from 'react';
import styles from "./styles.module.scss"

interface ILabel {
    text: string,
    isRequired: boolean,
}

const LabelForInput: FC<ILabel> = ({text, isRequired}) => {
    return (
        <p className={styles.text}>
            {text}
            {isRequired ? <span>*</span> : null}
        </p>
    );
};

export default LabelForInput;