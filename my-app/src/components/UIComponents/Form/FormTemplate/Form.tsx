import React, {FC, FormEventHandler, ReactNode} from 'react';
import BackButton from "../../Buttons/MainButton/BackButton";
import MainButton from "../../Buttons/MainButton/MainButton";
import SkipLink from "../../Buttons/Link/SkipLink";
import LogLink from "../../Buttons/Link/LogLink/LogLink";
import RegisterLink from "../../Buttons/Link/RegisterLink/RegisterLink";
import Box from "@mui/material/Box";
import styles from "./styles.module.scss"
import {SxProps} from "@mui/material";

interface IForm {
    backButton: boolean,
    title: string,
    onSubmit: FormEventHandler,
    children?: ReactNode,
    submitButtonText: string,
    additional?: true,
    addType?: "skip" | "log" | "register",
    marginTop: SxProps
}

const Form: FC<IForm> = ({onSubmit, backButton, title, submitButtonText, additional, addType, marginTop, children}) => {

    const additionalInfoCheck = (value: boolean | undefined): ReactNode | null => {
        return value
            ? addType === "skip"
                ? <SkipLink/>
                : addType === "register"
                    ? <RegisterLink/>
                    : addType === "log"
                        ? <LogLink/> : null
            : null
    }

    const backButtonCheck = (value: boolean): ReactNode | null => {
        return value ? <BackButton/>
            : null
    }

    return (
        <Box onSubmit={onSubmit} component={"form"} className={styles.form} sx={marginTop}>
            <div className={styles.wrapper}>
                {backButtonCheck(backButton)}
                <h3 className={styles.title}>{title}</h3>
            </div>
            <div>{children}</div>
            <MainButton
                sx={{width: "400px", height: "43px", borderRadius: "10px", fontSize: "16px"}}
                type={"submit"}
                modify={"contained"}>
                {submitButtonText}
            </MainButton>
            {additionalInfoCheck(additional)}
        </Box>
    );
};

export default Form;