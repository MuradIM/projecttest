import React, {useState} from 'react';
import Form from "../FormTemplate/Form";
import styles from "./styles.module.scss"
import PasswordInput from "../../Input/PasswordInput/PasswordInput";
import {FieldValues, SubmitHandler, useForm} from "react-hook-form";
import EmailInput from "../../Input/EmailInput/EmailInput";
import UsernameInput from "../../Input/UsernameInput/UsernameInput";
import LabelForInput from "../../label/LabelForInput/LabelForInput";
import {Link} from "react-router-dom";
import inputsValidation from "../../../../inputsValidation/errMessages";
import {schema} from "../../../../inputsValidation/validationPatterns/registration";

export interface IErr {
    hasErr: boolean,
    errMessage?: string,
}

const FormRegistration = () => {

    const {register, handleSubmit} = useForm()
    const [usernameErr, setUsernameErr] = useState<IErr>({hasErr: false})
    const [emailErr, setEmailErr] = useState<IErr>({hasErr: false})
    const [passwordErr, setPasswordErr] = useState<IErr>({hasErr: false})
    const [confirmPasswordErr, setConfirmPasswordErr] = useState<IErr>({hasErr: false})

    const onSubmit: SubmitHandler<FieldValues> = (data) => {
        console.log(data)
        const validData = inputsValidation(data, schema)

        if (typeof validData === "object") {
            validData.forEach(e => {
                if (e.path === "username") {
                    setUsernameErr({hasErr: true, errMessage: e.errMessage})
                } else if (e.path === "email") {
                    setEmailErr({hasErr: true, errMessage: e.errMessage})
                } else if (e.path === "password") {
                    setPasswordErr({hasErr: true, errMessage: e.errMessage})
                } else if (e.path === "passwordConfirm") {
                    setConfirmPasswordErr({hasErr: true, errMessage: e.errMessage})
                }
            })
        }
    }

    return (
        <Form
            title={"Register"}
            backButton={false}
            submitButtonText={"register"}
            marginTop={{marginTop: "48px"}}
            additional={true} addType={"log"}
            onSubmit={handleSubmit(onSubmit)}
        >
            <div className={styles.wrapper}>
                <LabelForInput
                    isRequired={true}
                    text="Username"/>
                <UsernameInput
                    err={usernameErr}
                    props={{...register("username")}}/>
            </div>
            <div className={styles.wrapper}>
                <LabelForInput
                    isRequired={true}
                    text="E-mail"/>
                <EmailInput
                    err={emailErr}
                    props={{...register("email")}}/>
            </div>
            <div className={styles.wrapper}>
                <LabelForInput
                    isRequired={true}
                    text="Create password"/>
                <PasswordInput
                    err={passwordErr}
                    props={{...register("password")}}/>
            </div>
            <div className={styles.wrapper}>
                <LabelForInput
                    isRequired={true}
                    text="Confirm password"/>
                <PasswordInput
                    err={confirmPasswordErr}
                    props={{...register("passwordConfirm")}}/>
            </div>
            <div className={styles.textWrapper}>
                <p className={styles.text}>
                    Your personal data will be used to support your
                    experience throughout this website, to manage access
                    to your account, and for other purposes described in our
                    <Link className={styles.link} to={"/"}> privacy policy</Link>.</p>
            </div>

        </Form>
    );
};

export default FormRegistration;