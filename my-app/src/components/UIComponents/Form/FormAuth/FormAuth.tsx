import React, {useState} from 'react';
import {Checkbox, FormControlLabel} from "@mui/material";
import styles from "./styles.module.scss";
import LabelForInput from "../../label/LabelForInput/LabelForInput";
import EmailInput from "../../Input/EmailInput/EmailInput";
import PasswordInput from "../../Input/PasswordInput/PasswordInput";
import Form from "../FormTemplate/Form";
import {FieldValues, SubmitHandler, useForm} from 'react-hook-form';
import inputsValidation from "../../../../inputsValidation/errMessages";
import RememberMeLabel from "../../label/labelRememberMe/RememberMeLabel";
import {Link} from "react-router-dom";
import {RESET_PASSWORD_PAGE} from "../../../../utils/consts";
import {schema} from "../../../../inputsValidation/validationPatterns/login";

export interface IErr {
    hasErr: boolean,
    errMessage?: string,
}

const FormAuth = () => {

    const {register, handleSubmit, setValue} = useForm()
    const [emailErr, setEmailErr] = useState<IErr>({hasErr: false})
    const [passwordErr, setPasswordErr] = useState<IErr>({hasErr: false})

    setValue("doRemember", false)

    const checkboxHandler = (e: any) => setValue("doRemember", e.target.checked)

    const onSubmit: SubmitHandler<FieldValues> = (data) => {

        const validData = inputsValidation(data, schema)

        if (typeof validData === "object") {
            validData.forEach(e => {
                if (e.path === "email") {
                    setEmailErr({hasErr: true, errMessage: e.errMessage})
                } else if (e.path === "password") {
                    setPasswordErr({hasErr: true, errMessage: e.errMessage})
                }
            })
        } else if (validData) {
            // fetch
            console.log(validData)
            console.log(data)
        }
    }

    return (
        <Form
            onSubmit={handleSubmit(onSubmit)}
            backButton={false}
            title={"Log In"}
            submitButtonText={"log in"}
            additional={true}
            addType={"register"}
            marginTop={{marginTop: "202px"}}
        >
            <div className={styles.wrapper}>
                <LabelForInput
                    isRequired={true}
                    text="Username or E-mail"/>
                <EmailInput
                    err={emailErr}
                    props={{...register("email")}}/>
            </div>
            <div className={styles.wrapper}>
                <LabelForInput
                    isRequired={true}
                    text="Password"/>
                <PasswordInput
                    err={passwordErr}
                    props={{...register("password")}}/>
                <Link className={styles.link} to={RESET_PASSWORD_PAGE}>Forgot password? </Link>
            </div>
            <FormControlLabel
                sx={{
                    marginTop: "-12px",
                    marginBottom: "36px"
                }}
                onChange={checkboxHandler}
                value="end"
                control={<Checkbox sx={{
                    color: "#3D4CEB",
                    '&.Mui-checked': {
                        color: "#3D4CEB",
                    },
                }}/>}
                label={<RememberMeLabel/>}
                labelPlacement="end"
            />
        </Form>
    )
}

export default FormAuth;