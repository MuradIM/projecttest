import React, {FC, MouseEventHandler} from 'react';
import styles from "./styles.module.scss";
import {NavigateFunction, useNavigate} from "react-router-dom";
import {HOME_PAGE} from "../../../utils/consts";

const Logo: FC = () => {
    const navigate:NavigateFunction = useNavigate()

    const clickHandler:MouseEventHandler = (): void => {
        navigate(`${HOME_PAGE}`)
    }


    return (
        <div onClick={clickHandler} className={styles.logoWrapper}>
            <div className={styles.square}>
                <div className={styles.squareSmall}>
                </div>
            </div>
            <div className={styles.textWrapper}>
                <p>agency</p>
                <h3>without borders</h3>
            </div>
        </div>
    );
};

export default Logo;