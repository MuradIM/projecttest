import React from 'react';
import FormControl from '@mui/material/FormControl';
import NativeSelect from '@mui/material/NativeSelect';
import ArrowDown from '@mui/icons-material/KeyboardArrowDownRounded';
import {SxProps} from "@mui/material";

const sxProps: SxProps = [
    {
        marginTop: "5px",
        fontSize: "16px",
        fontWeight: "normal",
        color: "#333333"
    }, {
        "&::before": {
            border: "none"
        }, "&::after": {
            border: "none"
        },
    },
]

const LanguageSwitchButton = () => {
    return (
        <>
            <FormControl>
                <NativeSelect
                    sx={sxProps}
                    variant={"outlined"}
                    defaultValue={"en"}
                    IconComponent={ArrowDown}
                >
                    <option value={"en"}>EN</option>
                    <option value={"rus"}>RUS</option>
                </NativeSelect>
            </FormControl>
        </>
    );
};

export default LanguageSwitchButton;