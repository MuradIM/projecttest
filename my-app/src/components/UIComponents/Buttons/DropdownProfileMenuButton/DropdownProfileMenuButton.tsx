import React from 'react';
import KeyboardArrowDownRoundedIcon from '@mui/icons-material/KeyboardArrowDownRounded';
import styles from "./styles.module.scss"

const DropdownProfileMenuButton = () => {
    return (
        <div className={styles.wrapper}>
            <KeyboardArrowDownRoundedIcon sx={{color: "#FFFFFF"}}/>
        </div>
    );
};

export default DropdownProfileMenuButton;