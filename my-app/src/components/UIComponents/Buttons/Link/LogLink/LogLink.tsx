import React from 'react';
import {Link} from "react-router-dom";
import {AUTH_PAGE} from "../../../../../utils/consts";
import styles from './styles.module.scss'

const LogLink = () => {
    return (
        <div className={styles.wrapper}>
            <p>Already have an Account?<Link className={styles.link} to={AUTH_PAGE}>Log In</Link></p>
        </div>
    );
};

export default LogLink;