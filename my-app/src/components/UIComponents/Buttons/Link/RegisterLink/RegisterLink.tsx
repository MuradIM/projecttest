import React from 'react';
import {Link} from "react-router-dom";
import {REGISTER_PAGE} from "../../../../../utils/consts";
import styles from './styles.module.scss'

const RegisterLink = () => {
    return (
        <div className={styles.wrapper}>
            <p>Dont have an Account?<Link className={styles.link} to={REGISTER_PAGE}>Register</Link></p>
        </div>
    );
};

export default RegisterLink;