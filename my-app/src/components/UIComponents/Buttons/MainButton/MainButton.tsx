import React, {FC, MouseEventHandler} from 'react';
import {Button, SxProps} from "@mui/material";

interface IButtonListProps {
    children: React.ReactNode,
    modify?: "text" | "outlined" | "contained",
    size?: "small" | "medium" | "large",
    href?: string,
    disabled?: true,
    fullWidth?: boolean,
    color?: 'inherit' | 'primary' | 'secondary' | 'success' | 'error' | 'info' | 'warning' | string,
    sx?: SxProps,
    onClick?: MouseEventHandler,
    type?: "button" | "submit" | "reset"
}

const MainButton: FC<IButtonListProps> = ({ type, modify, size, disabled, href,onClick , sx,children}) => {

    return (
        <Button type={type} onClick={onClick} sx={sx} href={href} variant={modify} size={size} disabled={disabled}>{children}</Button>
    );
};

export default MainButton