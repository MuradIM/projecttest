import React, {FC} from 'react';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {Button} from "@mui/material";

const BackButton: FC = () => {
    return (
        <Button startIcon={<ArrowBackIcon/>}>
            Back
        </Button>
    );
};

export default BackButton;