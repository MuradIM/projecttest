import React from 'react';
import NotificationsIcon from '@mui/icons-material/Notifications';
import {Badge, IconButton} from "@mui/material";

const NotificationButton = () => {
    return (
        <IconButton sx={{marginRight: "16px"}}>
            <Badge badgeContent={4} color={"error"}>
                <NotificationsIcon/>
            </Badge>
        </IconButton>
    );
};

export default NotificationButton;