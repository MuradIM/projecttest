import React, {FC} from 'react';
import {UseFormRegisterReturn} from "react-hook-form";
import {IErr} from "../../Form/FormAuth/FormAuth";
import TextField from "@mui/material/TextField";


interface InputProps {
    props: UseFormRegisterReturn,
    err: IErr,
}

const UsernameInput: FC<InputProps> = ({err, props}) => (

    <TextField error={err.hasErr}
               helperText={err.errMessage}
               inputProps={props}
               fullWidth
               label="Enter your username"
               variant="outlined"/>
)

export default UsernameInput;