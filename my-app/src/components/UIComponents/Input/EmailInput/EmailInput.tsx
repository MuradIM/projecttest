import React, {FC} from 'react';
import TextField from '@mui/material/TextField';
import {UseFormRegisterReturn} from "react-hook-form";
import {IErr} from "../../Form/FormAuth/FormAuth";

interface InputProps {
    props: UseFormRegisterReturn,
    err: IErr,
}

const EmailInput: FC<InputProps> = ({err, props}) => (

    <TextField error={err.hasErr}
               helperText={err.errMessage}
               inputProps={props}
               fullWidth
               label="example@gmail.com"
               variant="outlined"/>
)


export default EmailInput;