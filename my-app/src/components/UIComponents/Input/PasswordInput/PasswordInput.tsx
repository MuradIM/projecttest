import React, {MouseEventHandler, FC} from 'react';
import {FormControl, IconButton, InputAdornment, InputLabel, OutlinedInput, FormHelperText} from "@mui/material";
import {Visibility, VisibilityOff} from "@mui/icons-material";
import {UseFormRegisterReturn} from "react-hook-form";
import {IErr} from "../../Form/FormAuth/FormAuth";

interface InputProps {
    props: UseFormRegisterReturn,
    err: IErr
}

const PasswordInput: FC<InputProps> = ({err, props}) => {
    const [values, setValues] = React.useState({
        password: '',
        showPassword: false,
    });

    const handleChange = (prop: any) => (event: any) => {
        setValues({...values, [prop]: event.target.value})
    };

    const handleClickShowPassword: MouseEventHandler = () => {
        setValues({
            ...values,
            showPassword: !values.showPassword,
        });
    };

    return (
        <FormControl fullWidth variant="outlined">
            <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
            <OutlinedInput
                error={err.hasErr}
                inputProps={props}
                // id="outlined-adornment-password"
                type={values.showPassword ? 'text' : 'password'}
                value={values.password}
                onChange={handleChange('password')}
                endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            edge="end"
                        >
                            {values.showPassword ? <VisibilityOff/> : <Visibility/>}
                        </IconButton>
                    </InputAdornment>
                }
                label="Password"
            />
            {err.hasErr
                ? <FormHelperText error>{err.errMessage}</FormHelperText>
                : null
            }
        </FormControl>
    )
}

export default PasswordInput;