import React, {FC, ReactNode} from 'react';
import Logo from "../UIComponents/Logo/Logo";
import styles from "./styles.module.scss"
import Menu from "../SiteMap/Menu";
import {Link} from "react-router-dom";

const Footer: FC = () => {
    return (
        <footer className={styles.footer}>
            <Logo/>
            <div className={styles.infoSection}>
                <div className={styles.contactsWrapper}>
                    <div className={styles.contacts}>
                        <TextContent title={"Address"}>
                            <p className={styles.text}>980 N. Federal Hwy, Suite #110<br />Boca Raton, FL 33432</p>
                        </TextContent>
                        <TextContent title={"Phone"}>
                            <a className={styles.phoneLink} href="tel:5612838800">(561) 283 - 8800</ a>
                        </TextContent>
                    </div>
                    <TextContent title={"©2021 REALTOKEN"}>
                        <p className={styles.text}>
                            <Link className={styles.link} to={"/"}> Terms of Service</Link> and <Link
                            className={styles.link} to={"/"}>Privacy Policy.</Link>
                        </p>
                    </TextContent>
                </div>
                <div className={styles.menuSection}>
                    <h3 className={styles.title}>Site map</h3>
                    <Menu from={"footer"}/>
                </div>
            </div>
        </footer>
    );
};

interface ITextContent {
    title: string,
    children: ReactNode,
}


const TextContent: FC<ITextContent> = ({title, children}) => {
    return (
        <div className={styles.wrapper}>
            <h3 className={styles.title}>{title}</h3>
            {children}
        </div>
    )
}

export default Footer;