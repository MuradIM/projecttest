interface AuthArr {
    type: string,
    payload: boolean
}


export const isAuth = ():AuthArr => {
    return {
        type: "IS_AUTH",
        payload: true,
    }
}

export const isNotAuth = ():AuthArr => {
    return {
        type: "IS_NOT_AUTH",
        payload: false,
    }
}