import {IS_AUTH, NOT_AUTH} from "./types";


interface UserState {
    isAuth: boolean,
}

interface UserAction {
    type: string,
    payload?: any
}

const initialState: UserState = {isAuth: true}

export const userReducer = (state = initialState, action: UserAction): UserState => {
    switch (action.type) {
        case IS_AUTH:
            return {isAuth: true}
        case NOT_AUTH:
            return {isAuth: false}
        default:
            return state
    }
}

