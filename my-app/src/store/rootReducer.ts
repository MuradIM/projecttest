import {combineReducers} from "redux";
import {userReducer} from "./reducer";

export const rootReducer = combineReducers({
    userAuth: userReducer
})

export type RootState = ReturnType<typeof rootReducer>