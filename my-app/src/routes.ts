import {
    AUTH_PAGE,
    BLOG_PAGE,
    DASHBOARD_PAGE,
    HOME_PAGE,
    MARKETPLACE_PAGE,
    MY_INVESTMENTS_PAGE,
    REGISTER_PAGE, RESET_PASSWORD_PAGE,
    WALLET_PAGE
} from "./utils/consts";
import Wallet from "./pages/Wallet/Wallet";
import Dashboard from "./pages/Dashboard/Dashboard";
import Marketplace from "./pages/Marketplace/Marketplace";
import MyInvestments from "./pages/MyInvestments/MyInvestmsents";
import Home from "./pages/Home/Home";
import Blog from "./pages/Blog/Blog";
import Auth from "./pages/Auth/Auth";
import Register from "./pages/Register/Register";
import {FC} from "react";
import ResetPassword from "./pages/ResetPassword/ResetPassword";

interface IRoutes {
    path:string,
    Component: FC
}

export const authRoutes:IRoutes[] = [
    {
        path: DASHBOARD_PAGE,
        Component: Dashboard,
    }, {
        path: MY_INVESTMENTS_PAGE,
        Component: MyInvestments,
    }, {
        path: WALLET_PAGE,
        Component: Wallet,
    },
]

export const publicRoutes:IRoutes[] = [
    {
        path: HOME_PAGE,
        Component: Home,
    }, {
        path: BLOG_PAGE,
        Component: Blog,
    }, {
        path: AUTH_PAGE,
        Component: Auth,
    }, {
        path: REGISTER_PAGE,
        Component: Register,
    }, {
        path: RESET_PASSWORD_PAGE,
        Component: ResetPassword,
    }, {
        path: MARKETPLACE_PAGE,
        Component: Marketplace,
    },
]