// import {schema} from "./validationPatterns/login";
import * as z from "zod";

interface IErrArray {
    path: string | number,
    errMessage: string,
}

const inputsValidation = (data:any, schema: z.ZodObject<any>) => {
    try {
        schema.parse({...data})
        return true
    } catch (err) {
        if (err instanceof z.ZodError) {
          return err.issues.map((e): IErrArray => {
              const [path] = [...e.path]
              return {
                  path: path,
                  errMessage: e.message,
              }
          })
        }
    }
}

export default inputsValidation