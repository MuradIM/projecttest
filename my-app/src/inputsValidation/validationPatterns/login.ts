import * as z from "zod";

const regex = new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])")

export const schema = z.object({
    email: z.string()
        .email()
        .nonempty({message: "cant be empty"}),
    password: z.string()
        .regex(regex, {message: "Should be at least one digit, one capital and one lowercase letter"})
        .max(20, {message: "Max 20 symbols"})
        .min(8, {message: "Min 8 symbols"})
        .nonempty({message: "Cant be empty"}),
    doRemember: z.boolean().default(false)
})